This repositories contains my experiments with OpenGL

## Dependencies
- GLFW

## Build and Run
Requires Zig master
```
git clone --recurse-submodules git@codeberg.org:Gaarco/opengl-study.git
cd opengl-study
zig build run
```
